#ifndef QMLCALCULATOR_H
#define QMLCALCULATOR_H

#include "qmlcalculation.h"

#include <QObject>

class QMLCalculator : public QObject
{
    Q_OBJECT

public:
    explicit QMLCalculator(QObject *parent = nullptr);
    Q_INVOKABLE QMLCalculation *start(const QVariantMap &input,
                                      QMLCalculation::AnalysisType type,
                                      QMLCalculation::Mode mode = QMLCalculation::Synchronous);
};

#endif // QMLCALCULATOR_H
