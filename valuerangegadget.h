#ifndef VALUERANGEGADGET_H
#define VALUERANGEGADGET_H

#include "valuerange.h"

#include <QObject>

class ValueRangeGadget
{
    Q_GADGET
    Q_PROPERTY(RangeType type READ type STORED false FINAL)
    Q_PROPERTY(QVariant from READ from WRITE setFrom STORED false FINAL)
    Q_PROPERTY(QVariant to READ to WRITE setTo STORED false FINAL)
    Q_PROPERTY(QVariant step READ step WRITE setStep STORED false FINAL)

public:
    enum RangeType
    {
        InvalidRange = ValueRange::InvalidRange,
        RealRange = ValueRange::RealRange,
        IntegerRange = ValueRange::IntegerRange
    };
    Q_ENUM(RangeType)

    ValueRangeGadget(const ValueRange &data = ValueRange());

    inline operator ValueRange() const { return m_data; }

    RangeType type() const;
    QVariant from() const;
    QVariant to() const;
    QVariant step() const;

public slots:
    void setFrom(const QVariant &from);
    void setTo(const QVariant &to);
    void setStep(const QVariant &step);

private:
    ValueRange m_data;
};

Q_DECLARE_METATYPE(ValueRangeGadget)

#endif // VALUERANGEGADGET_H
