#ifndef BEAM_H
#define BEAM_H

#include <list>

struct Particle
{
    double x;
    double y;
    double z;
};

typedef std::list<Particle> Beam;

#endif // BEAM_H
