#ifndef NUMERICALCALCULATION_H
#define NUMERICALCALCULATION_H

#include "icalculation.h"

class NumericalCalculation final : public ICalculation
{
    friend class Calculator;

public:
    Physics::AnalysisType analysisiType() const override final;

protected:
    explicit NumericalCalculation(const StateChangeCallback &callback = StateChangeCallback());

    CalculationUnit calculate(const CalculationUnit &input,
                              bool &successful,
                              QString &errorText) override final;
};

#endif // NUMERICALCALCULATION_H
