#include "numericalcalculation.h"
#include "cascade.h"

Q_DECLARE_METATYPE(QList<Physics::NumericDetail>)

Physics::AnalysisType NumericalCalculation::analysisiType() const
{
    return Physics::Numerical;
}

NumericalCalculation::NumericalCalculation(const StateChangeCallback &callback) :
    ICalculation(callback)
{
}

CalculationUnit NumericalCalculation::calculate(const CalculationUnit &input,
                                                bool &successful,
                                                QString &errorText)
{
    bool ok;
    double harmonicNumber = input.value("harmonicNumber").toDouble(&ok);
    Q_ASSERT(ok);

    double U0 = input.value("U0").toDouble(&ok);
    Q_ASSERT(ok);

    double lambda = input.value("lambda").toDouble(&ok);
    Q_ASSERT(ok);

    double phaseCount = input.value("phaseCount").toInt(&ok);
    Q_ASSERT(ok);

    Physics::ModulationType modulation = static_cast<Physics::ModulationType>(input.value("modulation").toInt(&ok));
    Q_ASSERT(ok);

    QVariant variantCascades = input.value("cascades");
    ok = variantCascades.userType() == qMetaTypeId<QList<Cascade> >();
    QList<Cascade> cascades = variantCascades.value<QList<Cascade> >();
    Q_ASSERT(ok);

    if (!ok) {
        successful = false;
        errorText = QObject::tr("Invalid calculation input");
        return CalculationUnit();
    }

    double AOCC;
    CalculationUnit output;

    QVariant detailed = input.value("detail").toBool();
    if (detailed.isValid() && detailed.toBool()) {
        QList<Physics::NumericDetail> detail;
        AOCC = Physics::AOCCNumericDetail(harmonicNumber, U0, lambda, phaseCount, modulation,
                                          cascades, detail, successful);
        output.addValue("detail", QVariant::fromValue(detail));
    } else {
        AOCC = Physics::AOCCNumeric(harmonicNumber, U0, lambda, phaseCount,
                                    modulation, cascades, successful);
    }

    if (!successful) {
        errorText = QObject::tr("Calculation error");
        return CalculationUnit();
    }

    output.addValue("AOCC", AOCC);

    return output;
}
