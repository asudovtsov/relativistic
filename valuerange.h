#ifndef VALUERANGE_H
#define VALUERANGE_H

#include "rangedata.h"

#include <QVariant>
#include <QSharedDataPointer>

class ValueRange
{

public:
    enum RangeType
    {
        InvalidRange = RangeData::InvalidData,
        RealRange = RangeData::RealData,
        IntegerRange = RangeData::IntegerData
    };

    ValueRange();
    ValueRange(const ValueRange &other) = default;
    ValueRange &operator=(const ValueRange &other) = default;
    ~ValueRange();

    RangeType type() const;

    QVariant from() const;
    void setFrom(const QVariant &from);

    QVariant to() const;
    void setTo(const QVariant &to);

    QVariant step() const;
    void setStep(const QVariant &step);

    QVariant next(const QVariant &value) const;
    QVariant previous(const QVariant &value) const;

private:
    QSharedDataPointer<RangeData> m_data;
};

Q_DECLARE_METATYPE(ValueRange)

#endif // VALUERANGE_H
