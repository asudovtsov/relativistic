#include "beam2dpainteditem.h"

#include <QPainter>

#include <QtQml>

static void registerQmlTypes()
{
    qmlRegisterType<Beam2DPaintedItem>("relativistic", 1, 0, "Beam2DPaintedItem");
}

Q_COREAPP_STARTUP_FUNCTION(registerQmlTypes)

Beam2DPaintedItem::Beam2DPaintedItem(QQuickItem *parent) : QQuickPaintedItem(parent)
{

}

void Beam2DPaintedItem::paint(QPainter *painter)
{
    if (m_beam.userType() != qMetaTypeId<std::shared_ptr<Beam> >())
        return;

    auto beam = std::move(m_beam.value<std::shared_ptr<Beam> >());

    if (Q_UNLIKELY(!beam))
        return;

    for (auto particle : (*beam)) {
        painter->drawRoundRect(particle.x * contentsScale(),
                               particle.y * contentsScale(),
                               m_particleSize.height(),
                               m_particleSize.width(),
                               m_particleRadius,
                               m_particleRadius);
    }
}

QVariant Beam2DPaintedItem::beam() const
{
    return m_beam;
}

void Beam2DPaintedItem::setBeam(const QVariant &beam)
{
    m_beam = beam;
    emit beamChanged();
}

QSize Beam2DPaintedItem::particleSize() const
{
    return m_particleSize;
}

void Beam2DPaintedItem::setParticleSize(const QSize &particleSize)
{
    if (m_particleSize == particleSize)
        return;

    m_particleSize = particleSize;
    emit particleSizeChanged();
}

double Beam2DPaintedItem::particleRadius() const
{
    return m_particleRadius;
}

void Beam2DPaintedItem::setParticleRadius(const double &particleRadius)
{
    if (qFuzzyIsNull(m_particleRadius) && qFuzzyIsNull(particleRadius))
        return;

    if (qFuzzyCompare(m_particleRadius, particleRadius))
        return;

    m_particleRadius = particleRadius;
    emit particleRadiusChanged();
}
