#include "calculator.h"
#include "analyticalcalculation.h"
#include "numericalcalculation.h"

std::shared_ptr<ICalculation> Calculator::makeCalculation(Physics::AnalysisType type,
                                                const std::function<void()> &callback)
{
    switch (type) {
    case Physics::Analytical:
        return std::shared_ptr<ICalculation>(new AnalyticalCalculation(callback));
    case Physics::Numerical:
        return std::shared_ptr<ICalculation>(new NumericalCalculation(callback));
    default:
        Q_UNREACHABLE();
        break;
    }

    return std::shared_ptr<ICalculation>();
}
