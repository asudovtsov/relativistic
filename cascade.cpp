#include "cascade.h"

Cascade::Cascade() :
    m_leftTubeLength(0),
    m_rightTubeLength(0),
    m_ringRadius(0)
{
}

double Cascade::leftTubeLength() const
{
    return m_leftTubeLength;
}

void Cascade::setLeftTubeLength(double leftTubeLength)
{
    m_leftTubeLength = leftTubeLength;
}

double Cascade::rightTubeLength() const
{
    return m_rightTubeLength;
}

void Cascade::setRightTubeLength(double rightTubeLength)
{
    m_rightTubeLength = rightTubeLength;
}

double Cascade::linearLength() const
{
    return m_leftTubeLength + m_rightTubeLength;
}

double Cascade::totalLength() const
{
    return m_leftTubeLength + m_rightTubeLength + 2.0 * M_PI * m_ringRadius;
}

double Cascade::ringRadius() const
{
    return m_ringRadius;
}

void Cascade::setRingRadius(double ringRadius)
{
    m_ringRadius = ringRadius;
}

QList<Cascade::Modulation> Cascade::modulations() const
{
    return m_modulations;
}

Cascade::Modulation Cascade::modulation(int harmonicNumber) const
{
    Q_ASSERT(harmonicNumber >= 1 && harmonicNumber <= m_modulations.count());
    return m_modulations[harmonicNumber - 1];
}

void Cascade::addModulation(const Cascade::Modulation &modulation)
{
    m_modulations.append(modulation);
}

void Cascade::resetModulations()
{
    m_modulations.clear();
}
