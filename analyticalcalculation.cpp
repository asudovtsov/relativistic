#include "analyticalcalculation.h"
#include "cascade.h"
#include <QList>

Physics::AnalysisType AnalyticalCalculation::analysisiType() const
{
    return Physics::Analytical;
}

AnalyticalCalculation::AnalyticalCalculation(const StateChangeCallback &callback) :
    ICalculation(callback)
{
}

CalculationUnit AnalyticalCalculation::calculate(const CalculationUnit &input,
                                                 bool &successful,
                                                 QString &errorText)
{
    bool ok;
    double harmonicNumber = input.value("harmonicNumber").toDouble(&ok);
    Q_ASSERT(ok);

    double U0 = input.value("U0").toDouble(&ok);
    Q_ASSERT(ok);

    double lambda = input.value("lambda").toDouble(&ok);
    Q_ASSERT(ok);

    QVariant variantCascades = input.value("cascades");
    ok = variantCascades.userType() == qMetaTypeId<QList<Cascade> >();    
    QList<Cascade> cascades = variantCascades.value<QList<Cascade> >();
    Q_ASSERT(ok);

    if (!ok) {
        successful = false;
        errorText = QObject::tr("Invalid calculation input");
        return CalculationUnit();
    }

    QVariantMap xParameters;
    double AOCC = Physics::AOCCAnalytic(harmonicNumber, U0, lambda,
                                        cascades, xParameters, successful);


    if (!successful) {
        errorText = QObject::tr("Calculation error");
        return CalculationUnit();
    }

    CalculationUnit output;
    output.addValue("AOCC", AOCC);
    output.addValue("xParameters", xParameters);

    return output;
}
