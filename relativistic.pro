QT += quick
CONFIG += c++1z

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    calculationunit.cpp \
    icalculation.cpp \
    valuerange.cpp \
    rangedata.cpp \
    physics.cpp \
    analyticalcalculation.cpp \
    numericalcalculation.cpp \
    cascade.cpp \
    qmlcalculator.cpp \
    calculator.cpp \
    qmlcalculation.cpp \
    cascadegadget.cpp \
    valuerangegadget.cpp \
    beam2dpainteditem.cpp \
    beam2dphysics.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    physics.h \
    calculationunit.h \
    icalculation.h \
    valuerange.h \
    rangedata.h \
    analyticalcalculation.h \
    numericalcalculation.h \
    cascade.h \
    qmlcalculator.h \
    calculator.h \
    qmlcalculation.h \
    cascadegadget.h \
    valuerangegadget.h \
    beam.h \
    buncher.h \
    beam2dpainteditem.h \
    beam2dphysics.h
