#include "cascadegadget.h"

ModulatonGadget::ModulatonGadget(const Cascade::Modulation &modulation) :
    m_data(modulation)
{}

double ModulatonGadget::Um() const
{
    return m_data.Um;
}

double ModulatonGadget::M() const
{
    return m_data.M;
}

double ModulatonGadget::Ro() const
{
    return m_data.Ro;
}

double ModulatonGadget::Q() const
{
    return m_data.Q;
}

double ModulatonGadget::Y() const
{
    return m_data.Y;
}

void ModulatonGadget::setUm(const double &Um)
{
    m_data.Um = Um;
}

void ModulatonGadget::setM(const double &M)
{
    m_data.M = M;
}

void ModulatonGadget::setRo(const double &Ro)
{
    m_data.Ro = Ro;
}

void ModulatonGadget::setQ(const double &Q)
{
    m_data.Q = Q;
}

void ModulatonGadget::setY(const double &Y)
{
    m_data.Y = Y;
}

CascadeGadget::CascadeGadget(const Cascade &data) :
    m_data(data)
{}

double CascadeGadget::leftTubeLength() const
{
    return m_data.leftTubeLength();
}

double CascadeGadget::rightTubeLength() const
{
    return m_data.rightTubeLength();
}

double CascadeGadget::ringRadius() const
{
    return m_data.ringRadius();
}

QVariantList CascadeGadget::modulations() const
{
    QVariantList variantModulations;
    for (auto modulation : m_data.modulations())
        variantModulations.append(QVariant::fromValue(ModulatonGadget(modulation)));
    return variantModulations;
}

QVariant CascadeGadget::modulation(int harmonicNumber) const
{
    return QVariant::fromValue(ModulatonGadget(m_data.modulation(harmonicNumber)));
}

void CascadeGadget::addModulation(const ModulatonGadget &modulation)
{
    m_data.addModulation(modulation);
}

void CascadeGadget::resetModulations()
{
    m_data.resetModulations();
}

void CascadeGadget::setLeftTubeLength(const double &leftTubeLength)
{
    m_data.setLeftTubeLength(leftTubeLength);
}

void CascadeGadget::setRightTubeLength(const double &rightTubeLength)
{
    m_data.setRightTubeLength(rightTubeLength);
}

void CascadeGadget::setRingRadius(const double &ringRadius)
{
    m_data.setRingRadius(ringRadius);
}

void CascadeGadget::setModulations(const QVariantList &moduldations)
{
    for (const QVariant &variantModulation : moduldations)
        m_data.addModulation(variantModulation.value<ModulatonGadget>());
}
