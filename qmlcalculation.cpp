#include "qmlcalculation.h"
#include "cascadegadget.h"
#include "valuerangegadget.h"

#include <QVariant>
#include <QJSValue>
#include <QtQml>

static void qmlRegister()
{
    qmlRegisterUncreatableType<QMLCalculation>("relativistic", 1, 0, "Calculation", "Calculation creatable from C++ only");
}

Q_COREAPP_STARTUP_FUNCTION(qmlRegister)

class Morph
{
public:
    Morph();

    static CalculationUnit morph(const CalculationUnit &qmlUnit);

private:
    template <typename T, typename Source>
    static QVariant jsArrayToQList(const QVariant &array);
};

CalculationUnit Morph::morph(const CalculationUnit &qmlUnit)
{
    QVariant cascadesJsArray = qmlUnit.value("cascades");
    if (!cascadesJsArray.isValid())
        return qmlUnit;

    QVariant morphed = jsArrayToQList<Cascade, CascadeGadget>(cascadesJsArray);
    if (!morphed.isValid())
        return CalculationUnit::Invalid;

    QVariantMap map = qmlUnit.toVariantMap();
    map.insert("cascades", morphed);

    for (QVariantMap::iterator it = map.begin(); it != map.end(); ++it)
        if (it.value().userType() == qMetaTypeId<ValueRangeGadget>())
            it.value() = QVariant::fromValue(ValueRange(it.value().value<ValueRangeGadget>()));

    return CalculationUnit::fromVariantMap(map);
}

template<typename Target, typename Source>
QVariant Morph::jsArrayToQList(const QVariant &array)
{
    if (array.userType() == qMetaTypeId<QList<Target> >())
        return array;

    if (array.userType() == qMetaTypeId<QJSValue>())
        return jsArrayToQList<Target, Source>(array.value<QJSValue>().toVariant());

    if (array.userType() == qMetaTypeId<Source>())
        return QVariant::fromValue(QList<Target>( { array.value<Source>() } ));

    if (array.userType() == QMetaType::QVariantList
            || array.userType() == qMetaTypeId<QList<Source> >()) {
        QList<Target> list;
        for (QVariant element : array.toList())
            list.append(element.value<Source>());
        return QVariant::fromValue(list);
    }

    return QVariant::Invalid;
}

bool QMLCalculation::isValid() const
{
    return m_analysisType != Invalid;
}

void QMLCalculation::start(const CalculationUnit &input, QMLCalculation::Mode mode)
{
    if (Q_UNLIKELY(!isValid()))
        return;

    m_data->start(Morph::morph(input), static_cast<ICalculation::Mode>(mode));
}

QMLCalculation::AnalysisType QMLCalculation::analysisType() const
{
    return m_analysisType;
}

QString QMLCalculation::errorText() const
{
    return isValid() ? m_data->errorText() : "Calculation object is invalid";
}

QMLCalculation::State QMLCalculation::state() const
{
    return isValid() ? static_cast<State>(m_data->state()) : Null;
}

CalculationUnit QMLCalculation::input() const
{
    return isValid() ? m_data->input() : CalculationUnit();
}

CalculationUnit QMLCalculation::output() const
{
    return isValid() ? m_data->output() : CalculationUnit();
}

QMLCalculation::QMLCalculation(QObject *parent) :
    QObject(parent),
    m_analysisType(Invalid)
{
}

void QMLCalculation::setData(std::shared_ptr<ICalculation> data)
{
    Q_ASSERT(!m_data);
    m_data = std::move(data);
    m_analysisType = m_data ? static_cast<AnalysisType>(m_data->analysisiType()) : Invalid;
}

void QMLCalculation::onDataStateChanged()
{
    emit stateChanged();
    emit errorTextChanged();
}
