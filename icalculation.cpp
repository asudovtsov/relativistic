#include "icalculation.h"

#include <QObject>

#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QFutureWatcher>

static void registerAnalysisType()
{
    qRegisterMetaType<Physics::AnalysisType>("AnalysisType");
}

Q_COREAPP_STARTUP_FUNCTION(registerAnalysisType)

ICalculation::~ICalculation()
{
}

void ICalculation::start(const CalculationUnit &input, Mode mode)
{
    if (m_state == Started)
        return;

    switch (mode) {
    case Synchronous: {
        bool successful;
        QString errorText;
        setState(Started);
        m_input = input;
        m_output = calculate(input, successful, errorText);
        m_errorText = errorText;
        setState(successful ? Finished : Error);
        break;
    }
    case Asynchronous: {
        struct FutureResult
        {
            bool successful;
            QString errorText;
            CalculationUnit output;
        };

        QFutureWatcher<FutureResult> *watcher = new QFutureWatcher<FutureResult>;
        QObject::connect(watcher, &QFutureWatcher<FutureResult>::finished, [this, watcher]() {
            FutureResult result = watcher->result();
            m_errorText = result.errorText;
            m_output = result.output;
            this->setState(result.successful ? Finished : Error);
            delete watcher;
        });

        m_input = input;
        m_output.reset();
        m_errorText.clear();

        setState(Started);

        auto pointer = shared_from_this();
        watcher->setFuture(QtConcurrent::run([this, input, pointer](){
            bool successful;
            QString errorText;
            CalculationUnit output = calculate(input, successful, errorText);
            FutureResult result = { successful, errorText, output };
            return result;
        }));

        break;
    }
    default:
        break;
    }
}

ICalculation::State ICalculation::state() const
{
    return m_state;
}

QString ICalculation::errorText() const
{
    return m_errorText;
}

CalculationUnit ICalculation::input() const
{
    return m_input;
}

CalculationUnit ICalculation::output() const
{
    return m_output;
}


ICalculation::ICalculation(const StateChangeCallback &callback) :
    m_state(Idle),
    m_callback(callback)
{
}

void ICalculation::setState(ICalculation::State state)
{
    if (m_state == state)
        return;

    m_state = state;
    m_callback();
}
