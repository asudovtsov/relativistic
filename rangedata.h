#ifndef RANGEDATA_H
#define RANGEDATA_H

#include <QVariant>
#include <QSharedData>

class RangeData : public QSharedData
{
public:
    enum RangeDataType
    {
        InvalidData,
        RealData,
        IntegerData
    };

    virtual ~RangeData();
    virtual RangeDataType type() const;
    virtual QVariant from() const;
    virtual void setFrom(const QVariant &from);
    virtual QVariant to() const;
    virtual void setTo(const QVariant &to);
    virtual QVariant step() const;
    virtual void setStep(const QVariant &step);
    virtual QVariant next(const QVariant &value) const;
    virtual QVariant previous(const QVariant &value) const;
};

class RealRangeData : public RangeData
{
public:
    ~RealRangeData() override final;
    RangeDataType type() const;
    QVariant from() const override final;
    void setFrom(const QVariant &from) override final;
    QVariant to() const override final;
    void setTo(const QVariant &to) override final;
    QVariant step() const override final;
    void setStep(const QVariant &step) override final;
    QVariant next(const QVariant &value) const override final;
    QVariant previous(const QVariant &value) const override final;

private:
    double m_from = 0.0;
    double m_to = 0.0;
    double m_step = 0.0;
};

class IntegerRangeData : public RangeData
{
public:
    ~IntegerRangeData() override final;
    RangeDataType type() const;
    QVariant from() const override final;
    void setFrom(const QVariant &from) override final;
    QVariant to() const override final;
    void setTo(const QVariant &to) override final;
    QVariant step() const override final;
    void setStep(const QVariant &step) override final;
    QVariant next(const QVariant &value) const override final;
    QVariant previous(const QVariant &value) const override final;

private:
    int m_from = 0;
    int m_to = 0;
    int m_step = 0;
};

#endif // RANGEDATA_H
