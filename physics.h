#ifndef PHYSICS_H
#define PHYSICS_H

class Cascade;
class QString;
class QVariant;
template <typename T> class QList;
template <typename K, typename T> class QMap;
typedef QMap<QString, QVariant> QVariantMap;

namespace Physics
{

enum AnalysisType
{
    Analytical,
    Numerical
};

struct NumericDetail
{
    double dzeta0Ring; // максимальный статический угол пролёта повортного кольца
    double dzeta0L1;   // максимальный статический угол пролёта первого линейного участка
    double dzeta0L2;   // максимальный статический угол пролёта второго линейного участка
    double dgammaMax;  // максимальное изменение энергии
    double dvMax;      // максимальное изменение скорости
    double drMax;      // максимальное изменение радиусса пролёта повортного кольца
    double AOCC;       // Амплитуда 1-ой гармоники конвекционного тока
};

enum ModulationType
{
    SinusModulation,
    SecondHarmonicModulation,
    ReverseSawModulation
};

constexpr double SPEED_OF_LIGHT = 299792458.0;
constexpr double ELECTRON_MASS = 9.10938356e-31;
constexpr double ELECTRON_CHARGE = 1.60217662e-19;

double gamma0(const double &U0);
double v0(const double &U0);
double r0(const double &v0, const double &gamma0, const double &B);
double betta0(const double &v0);

double dgamma(const double &Um);
double dr(const double &r0, const double &gamma0, const double &dGamma);
double r(const double &r0, const double &gamma0, const double &dGamma);
double dv(const double &v0, const double &gamma0, const double &dGamma);
double v(const double &gamma0, const double &dGamma);

double B(const double &r0, const double &gamma0);
double frequency(const double &lambda);
double period(const double &lambda);
double sinusModulation(const double &phase, const double &Um1, const double &m1);
double rsawModulation(const double &phase, const double &Um1, const double &m1);
double secondHarmonicModulation(const double &phase,
                                const double &Um1,
                                const double &Um2,
                                const double &m1,
                                const double &m2);

// Амплитуда n-ой гармоники конвекционного тока
// Amplitude of the n-th harmonic of convection current (AOCC)
double AOCCAnalytic(int harmonicNumber,
                    const double &U0,
                    const double &lambda,
                    const QList<Cascade> &cascades,
                    QVariantMap &xParameters,
                    bool &successful);

double AOCCNumeric(int harmonicNumber,
                   const double &U0,
                   const double &lambda,
                   int phaseCount,
                   ModulationType modulationType,
                   const QList<Cascade> &cascades,
                   bool &successful);

double AOCCNumericDetail(int harmonicNumber,
                         const double &U0,
                         const double &lambda,
                         int phaseCount,
                         ModulationType modulationType,
                         const QList<Cascade> &cascades,
                         QList<NumericDetail> &detail,
                         bool &successful);
} // Physics

#endif // PHYSICS_H
