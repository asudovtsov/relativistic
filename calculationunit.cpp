#include "calculationunit.h"
#include "valuerange.h"
#include <QSet>

CalculationUnit::CalculationUnit() :
    m_isValid(true),
    m_isRange(false)
{

}

CalculationUnit::CalculationUnit(const InvalidUnit &) :
    m_isValid(false),
    m_isRange(false)
{

}

QList<QString> CalculationUnit::valueNames() const
{
    return m_valueMap.keys();
}

bool CalculationUnit::isValid() const
{
    return m_isValid;
}

bool CalculationUnit::isRange() const
{
    return m_isRange;
}

bool CalculationUnit::isEmpty() const
{
    return m_valueMap.isEmpty();
}

QVariant CalculationUnit::value(const QString &name) const
{
    return m_valueMap.value(name, QVariant::Invalid);
}

bool CalculationUnit::addValue(const QString &name, const QVariant &value)
{
    if (Q_UNLIKELY(m_valueMap.contains(name)))
        return false;

    m_valueMap.insert(name, value);

    if (m_isValid)
        m_isValid = value.isValid();

    if (!m_isRange && value.userType() == qMetaTypeId<ValueRange>())
        m_isRange = true;

    return true;
}

void CalculationUnit::reset()
{
    m_valueMap.clear();
    m_isValid = true;
    m_isRange = false;
}

bool CalculationUnit::containsValue(const QString &name, QMetaType::Type type)
{
    return type == QMetaType::UnknownType
            ? m_valueMap.contains(name)
            : m_valueMap.value(name).userType() == type;
}

CalculationUnit CalculationUnit::fromVariantMap(const QVariantMap &map)
{
    CalculationUnit unit;
    unit.m_valueMap = map;
    return unit;
}

QVariantMap CalculationUnit::toVariantMap() const
{
    return m_valueMap;
}
