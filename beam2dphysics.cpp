#include "beam2dphysics.h"

#include "buncher.h"

namespace Beam2DPhysics {

Model::Model(unsigned phaseCount, unsigned cascadeCount) :
    m_phaseCount(phaseCount),
    m_cascadeCount(cascadeCount),
    m_r(new double[phaseCount * cascadeCount]),
    m_v(new double[phaseCount * cascadeCount]),
    m_t(new double[phaseCount * cascadeCount * 3]), // 3 it's section count
    m_dzetaTotal(new double[phaseCount])
{
}

Model::~Model()
{
    delete[] m_r;
    delete[] m_v;
    delete[] m_t;
    delete[] m_dzetaTotal;
}

unsigned Model::phaseCount() const
{
    return m_phaseCount;
}

unsigned Model::cascadeCount() const
{
    return m_cascadeCount;
}

double Model::r(unsigned phase, unsigned cascade) const
{
    return m_r[phase + cascade * cascadeCount()];
}

double Model::v(unsigned phase, unsigned cascade) const
{
    return m_v[phase + cascade * cascadeCount()];
}

double Model::t(unsigned phase, Model::Section, unsigned cascade) const
{
    return m_t[phase + cascade * cascadeCount()];
}

double Model::totalDzeta(unsigned phase) const
{
    return m_dzetaTotal[phase];
}

Model *makeBeamPhysicsModel(Physics::ModulationType modulationType,
                            unsigned phaseCount,
                            const Buncher &buncher)
{
}

void model(int ns, const Beam &beam, Model *physicsModel)
{
}

}
