#include "rangedata.h"

RangeData::~RangeData()
{
}

RangeData::RangeDataType RangeData::type() const
{
    return InvalidData;
}

QVariant RangeData::from() const
{
    return QVariant::Invalid;
}

void RangeData::setFrom(const QVariant &from)
{
    Q_UNUSED(from)
}

QVariant RangeData::to() const
{
    return QVariant::Invalid;
}

void RangeData::setTo(const QVariant &to)
{
    Q_UNUSED(to)
}

QVariant RangeData::step() const
{
    return QVariant::Invalid;
}

void RangeData::setStep(const QVariant &step)
{
    Q_UNUSED(step)
}

QVariant RangeData::next(const QVariant &value) const
{
    Q_UNUSED(value)
    return QVariant::Invalid;
}

QVariant RangeData::previous(const QVariant &value) const
{
    Q_UNUSED(value)
    return QVariant::Invalid;
}

RealRangeData::~RealRangeData()
{
}

RangeData::RangeDataType RealRangeData::type() const
{
    return RealData;
}

QVariant RealRangeData::from() const
{
    return m_from;
}

void RealRangeData::setFrom(const QVariant &from)
{
    if (Q_UNLIKELY(from.type() != QVariant::Double))
        return;

    m_from = from.toDouble();
}

QVariant RealRangeData::to() const
{
    return m_to;
}

void RealRangeData::setTo(const QVariant &to)
{
    if (Q_UNLIKELY(to.type() != QVariant::Double))
        return;

    m_to = to.toDouble();
}

QVariant RealRangeData::step() const
{
    return m_step;
}

void RealRangeData::setStep(const QVariant &step)
{
    if (Q_UNLIKELY(step.type() != QVariant::Double))
        return;

    m_step = step.toDouble();
}

QVariant RealRangeData::next(const QVariant &value) const
{
    if (Q_UNLIKELY(value.type() != QVariant::Double))
        return QVariant::Invalid;

    return value.toDouble() + m_step;
}

QVariant RealRangeData::previous(const QVariant &value) const
{
    if (Q_UNLIKELY(value.type() != QVariant::Double))
        return QVariant::Invalid;

    return value.toDouble() - m_step;
}

IntegerRangeData::~IntegerRangeData()
{
}

RangeData::RangeDataType IntegerRangeData::type() const
{
    return IntegerData;
}

QVariant IntegerRangeData::from() const
{
    return m_from;
}

void IntegerRangeData::setFrom(const QVariant &from)
{
    if (Q_UNLIKELY(from.type() != QVariant::Int))
        return;

    m_from = from.toInt();
}

QVariant IntegerRangeData::to() const
{
    return m_to;
}

void IntegerRangeData::setTo(const QVariant &to)
{
    if (Q_UNLIKELY(to.type() != QVariant::Int))
        return;

    m_to = to.toInt();
}

QVariant IntegerRangeData::step() const
{
    return m_step;
}

void IntegerRangeData::setStep(const QVariant &step)
{
    if (Q_UNLIKELY(step.type() != QVariant::Int))
        return;

    m_step = step.toInt();
}

QVariant IntegerRangeData::next(const QVariant &value) const
{
    if (Q_UNLIKELY(value.type() != QVariant::Int))
        return QVariant::Invalid;

    return value.toInt() + m_step;
}

QVariant IntegerRangeData::previous(const QVariant &value) const
{
    if (Q_UNLIKELY(value.type() != QVariant::Int))
        return QVariant::Invalid;

    return value.toInt() - m_step;
}
