#ifndef CALCULATIONUNIT_H
#define CALCULATIONUNIT_H

#include <QVariantMap>

class CalculationUnit
{
public:
    enum InvalidUnit { Invalid };

    CalculationUnit();
    CalculationUnit(const InvalidUnit &);

    QList<QString> valueNames() const;
    bool isValid() const;
    bool isRange() const;
    bool isEmpty() const;

    QVariant value(const QString &name) const;
    bool addValue(const QString &name, const QVariant &value);
    void reset();
    bool containsValue(const QString &name, QMetaType::Type type = QMetaType::UnknownType);

    static CalculationUnit fromVariantMap(const QVariantMap &map);
    QVariantMap toVariantMap() const;

private:
    QVariantMap m_valueMap;
    bool m_isValid;
    bool m_isRange;
};

#endif // CALCULATIONUNIT_H
