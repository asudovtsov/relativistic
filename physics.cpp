#include "physics.h"
#include "cascade.h"
#include <cmath>
#include <tr1/cmath>
#include <QtGlobal>
#include <QVariantMap>

namespace Physics
{

using std::pow;
using std::sqrt;
using std::sin;
using std::cos;

double gamma0(const double &U0)
{
    return 1.0 + (ELECTRON_CHARGE / (ELECTRON_MASS * pow(SPEED_OF_LIGHT, 2))) * U0;
}

double v0(const double &U0)
{
    return !qFuzzyIsNull(U0)
            ? sqrt(1
                   - pow(ELECTRON_MASS, 2)
                   * pow(SPEED_OF_LIGHT, 4)
                   / pow(ELECTRON_CHARGE * U0 + ELECTRON_MASS * pow(SPEED_OF_LIGHT, 2), 2))
              * SPEED_OF_LIGHT
            : 0;
}

double r0(const double &v0, const double &gamma0, const double &B)
{
    return !qFuzzyIsNull(B)
            ? ELECTRON_MASS * v0 * gamma0 / ELECTRON_CHARGE / B
            : 0;
}

double betta0(const double &v0)
{
    return v0 / SPEED_OF_LIGHT;
}

double dgamma(const double &Um)
{
    return !qFuzzyIsNull(Um)
            ? ELECTRON_CHARGE / (ELECTRON_MASS * pow(SPEED_OF_LIGHT, 2)) * Um
            : 0;
}

double dr(const double &r0, const double &gamma0, const double &dGamma)
{
    Q_ASSERT(gamma0 != 0);
    return r0 * dGamma / gamma0;
}

double r(const double &r0, const double &gamma0, const double &dGamma)
{
    Q_ASSERT(gamma0 != 0);
    return r0 + r0 * dGamma / gamma0;
}

double dv(const double &v0, const double &gamma0, const double &dGamma)
{
    return !qFuzzyIsNull(gamma0 + dGamma)
            ? v0 - SPEED_OF_LIGHT * sqrt(pow(gamma0 + dGamma, 2) - 1.0) / (gamma0 + dGamma)
            : 0;
}

double v(const double &gamma0, const double &dGamma)
{
    return !qFuzzyIsNull(gamma0 + dGamma)
            ? SPEED_OF_LIGHT * sqrt(pow(gamma0 + dGamma, 2) - 1.0) / (gamma0 + dGamma)
            : 0;
}

double B(const double &r0, const double &gamma0)
{
    return !qFuzzyIsNull(r0)
            ? ELECTRON_MASS * SPEED_OF_LIGHT * sqrt(pow(gamma0, 2) - 1.0) / ELECTRON_CHARGE / r0
            : 0;
}

double frequency(const double &lambda)
{
    Q_ASSERT(!qFuzzyIsNull(lambda));
    return SPEED_OF_LIGHT / lambda;
}

double period(const double &lambda)
{
    Q_ASSERT(!qFuzzyIsNull(lambda));
    return 1 / SPEED_OF_LIGHT / lambda;
}

double sinusModulation(const double &phase, const double &Um1, const double &m1)
{
    return Um1 * sin(phase) * m1;
}

double rsawModulation(const double &phase, const double &Um1, const double &m1)
{
    return Um1 * ((-phase + M_PI) / M_PI) * m1;
}

double secondHarmonicModulation(const double &phase,
                                const double &Um1,
                                const double &Um2,
                                const double &m1,
                                const double &m2)
{
    return Um2 * sin(2.0 * phase) * m2 - Um1 * sin(phase) * m1;
}

double calcAOCCAnalyticByX(double X[], int cascadeCount, int harmonicNumer)
{
    double AOCC = 0.0;
    double addendum = 0.0;
    double deduction = 0.0;
    auto getX = [&X, &cascadeCount](uint i, uint j) { return X[cascadeCount * i + j]; };

    for (int i = 1; i < cascadeCount; ++i) {
        addendum = std::tr1::cyl_bessel_j(harmonicNumer, harmonicNumer * getX(i, cascadeCount + 1));
        for (int j = 1; j < cascadeCount; ++j) {
            if (Q_UNLIKELY(i == j))
                continue;
            deduction = i < j ? getX(j, i) : 0;
            addendum *= std::tr1::cyl_bessel_j(0, getX(j, cascadeCount + 1) - deduction);
        }
        AOCC += addendum;
    }

    return AOCC;
}

double AOCCAnalytic(int harmonicNumber,
                    const double &U0,
                    const double &lambda,
                    const QList<Cascade> &cascades,
                    QVariantMap &xParameters,
                    bool &successful)
{

    if (cascades.isEmpty() || harmonicNumber <= 0 || qFuzzyIsNull(U0) || qFuzzyIsNull(lambda)) {
        successful = false;
        return 0.0;
    }

    QVariantMap xParametersMap;

    const double gamma0 = Physics::gamma0(U0);
    const double v0 = Physics::v0(U0);
    const double omega = 2.0 * M_PI * Physics::frequency(lambda);

    int XCount = cascades.count() * (cascades.count() + 1) / 2;
    double *X = new double[XCount]; // парциальные параметры
    double dzetar = 0.0;            // угол пролёта поворотного кольца
    double dzetal = 0.0;            // угол пролёта линейного участка
    double nur = 0.0;               // коэффициент скоростной модуляции в поворотном кольце
    double nul = 0.0;               // коэффициент скоростной модуляции на линейном участке

    int n = 0;
    int m = 1;
    for (int i = 0; i < XCount; ++i) {

        if (harmonicNumber > cascades[n].modulations().size()) {
            successful = false;
            delete[] X;
            return 0.0;
        }

        dzetar = 0;
        dzetal = 0;
        const Cascade::Modulation mod = cascades[n].modulation(1); // TODO задействовать номер гармоники

        for (int j = n; j < m; ++j) {
            dzetar += omega * cascades[j].ringRadius() * 2.0 * M_PI / v0;
            dzetal += omega * cascades[j].linearLength() / v0;
        }

        nur = mod.M * (mod.Um / U0) * (2.0 * (gamma0 - 1.0) / gamma0) / 2.0;
        nul = mod.M * (mod.Um / U0) * (2.0 / (gamma0 * (gamma0 + 1.0))) / 2.0;
        X[i] = dzetar * nur - dzetal * nul;
        xParametersMap.insert(QString("X%1%2").arg(n + 1).arg(m + 1), X[i]);

        ++m;
        if (m == (cascades.count() + 1)) {
            ++n;
            m = n + 1;
        }
    }

    double AOCC = calcAOCCAnalyticByX(X, cascades.count(), harmonicNumber);

    delete[] X;

    xParameters = xParametersMap;
    successful = true;

    return AOCC;
}

double AOCCNumeric(int harmonicNumber,
                   const double &U0,
                   const double &lambda,
                   int phaseCount,
                   ModulationType modulationType,
                   const QList<Cascade> &cascades,
                   bool &successful)
{
    if (phaseCount == 0
            || harmonicNumber <= 0
            || cascades.isEmpty()
            || qFuzzyIsNull(U0)
            || qFuzzyIsNull(lambda)) {
        successful = false;
        return 0.0;
    }

    const double v0 = Physics::v0(U0);
    const double gamma0 = Physics::gamma0(U0);
    const double omega = 2.0 * M_PI * Physics::frequency(lambda);
    const double phaseStep = 2.0 * M_PI / phaseCount;

    double *r = new double[phaseCount];
    double *v = new double[phaseCount];
    double *dzeta = new double[phaseCount];
    double *phase = new double[phaseCount];
    double *dgamma = new double[phaseCount];
    double *Um = new double[phaseCount];

    std::fill(dzeta, dzeta + phaseCount, 0);
    std::fill(dgamma, dgamma + phaseCount, 0);
    std::fill(Um, Um + phaseCount, 0);

    double sinSumm = 0.0;
    double cosSumm = 0.0;
    double dzeta0 = 0.0;

    for (int i = 0; i < cascades.count(); ++i) {

        if (harmonicNumber > cascades[i].modulations().size()) {
            successful = false;
            delete[] v;
            delete[] r;
            delete[] dzeta;
            delete[] phase;
            delete[] dgamma;
            delete[] Um;
            return 0.0;
        }

        const Cascade::Modulation mod1 = cascades[i].modulation(1); // TODO задействовать номер гармоники
        dzeta0 = (i != 0 ? omega * (cascades[i - 1].totalLength() / v0) : 0);

        for (int j = 0; j < phaseCount; ++j) {
            phase[j] = (i != 0 ? phase[j] + dzeta[j] - dzeta0 : phaseStep * j);

            switch (modulationType) {
            case SinusModulation: {
                Um[j] += sinusModulation(phase[j], mod1.Um, mod1.M);
                break;
            }
            case SecondHarmonicModulation: {
                const Cascade::Modulation mod2 = cascades[i].modulation(2); // TODO задействовать номер гармоники
                Um[j] += secondHarmonicModulation(phase[j],
                                                  mod1.Um,
                                                  mod2.Um,
                                                  mod1.M,
                                                  mod2.M);
                break;
            }
            case ReverseSawModulation: {
                Um[j] += rsawModulation(phase[j], mod1.Um, mod1.M);
                break;
            }
            default:
                break;
            }

            dgamma[j] = Physics::dgamma(Um[j]);
            r[j] = Physics::r(cascades[i].ringRadius(), gamma0, dgamma[j]);
            v[j] = Physics::v(gamma0, dgamma[j]);

            dzeta[j] = omega * (cascades[i].linearLength() + r[j] * 2.0 * M_PI) / v[j];

            if (i == cascades.count() - 1) {
                sinSumm += sin(phase[j] + dzeta[j] - omega * (cascades[i].totalLength()) / v0);

                cosSumm += cos(phase[j] + dzeta[j] - omega * (cascades[i].totalLength()) / v0);

                if (qFuzzyIsNull(sinSumm))
                    sinSumm = 0;

                if (qFuzzyIsNull(cosSumm))
                    cosSumm = 0;
            }
        }
    }

    delete[] v;
    delete[] r;
    delete[] dzeta;
    delete[] phase;
    delete[] dgamma;
    delete[] Um;

    successful = true;

    return sqrt(pow(2.0 / phaseCount * sinSumm, 2) + pow(2.0 / phaseCount * cosSumm, 2));
}

double AOCCNumericDetail(int harmonicNumber,
                         const double &U0,
                         const double &lambda,
                         int phaseCount,
                         ModulationType modulationType,
                         const QList<Cascade> &cascades,
                         QList<NumericDetail> &detail,
                         bool &successful)
{
    if (phaseCount == 0
            || harmonicNumber <= 0
            || cascades.isEmpty()
            || qFuzzyIsNull(U0)
            || qFuzzyIsNull(lambda)) {
        successful = false;
        return 0.0;
    }

    const double v0 = Physics::v0(U0);
    const double gamma0 = Physics::gamma0(U0);
    const double omega = 2.0 * M_PI * Physics::frequency(lambda);
    const double phaseStep = 2.0 * M_PI / phaseCount;

    double *r = new double[phaseCount];
    double *v = new double[phaseCount];
    double *dzeta = new double[phaseCount];
    double *phase = new double[phaseCount];
    double *dgamma = new double[phaseCount];
    double *Um = new double[phaseCount];

    std::fill(dzeta, dzeta + phaseCount, 0);
    std::fill(dgamma, dgamma + phaseCount, 0);
    std::fill(Um, Um + phaseCount, 0);

    double sinSumm = 0.0;
    double cosSumm = 0.0;
    double dzeta0 = 0.0;
    double AOCC = 0.0;
    double dvMax = 0.0;
    double drMax = 0.0;
    double dgammaMax = 0.0;

    QList<NumericDetail> detailList;

    for (int i = 0; i < cascades.count(); ++i) {

        if (harmonicNumber > cascades[i].modulations().size()) {
            successful = false;
            delete[] v;
            delete[] r;
            delete[] dzeta;
            delete[] phase;
            delete[] dgamma;
            delete[] Um;
            return 0.0;
        }

        const Cascade::Modulation mod1 = cascades[i].modulation(1); // TODO задействовать номер гармоники
        dzeta0 = (i != 0 ? omega * (cascades[i - 1].totalLength() / v0) : 0);
        AOCC = 0;
        sinSumm = 0;
        cosSumm = 0;
        dgammaMax = 0;
        dvMax = 0;
        drMax = 0;

        for (int j = 0; j < phaseCount; ++j) {
            phase[j] = (i != 0 ? phase[j] + dzeta[j] - dzeta0 : phaseStep * j);

            switch (modulationType) {
            case SinusModulation: {
                Um[j] += sinusModulation(phase[j], mod1.Um, mod1.M);
                break;
            }
            case SecondHarmonicModulation: {
                const Cascade::Modulation mod2 = cascades[i].modulation(2); // TODO задействовать номер гармоники
                Um[j] += secondHarmonicModulation(phase[j],
                                                  mod1.Um,
                                                  mod2.Um,
                                                  mod1.M,
                                                  mod2.M);
                break;
            }
            case ReverseSawModulation: {
                Um[j] += rsawModulation(phase[j], mod1.Um, mod1.M);
                break;
            }
            default:
                break;
            }

            dgamma[j] = Physics::dgamma(Um[j]);
            r[j] = Physics::r(cascades[i].ringRadius(), gamma0, dgamma[j]);
            v[j] = Physics::v(gamma0, dgamma[j]);
            dzeta[j] = omega * (cascades[i].linearLength() + r[j] * 2.0 * M_PI) / v[j];

            if (dgammaMax < dgamma[j])
                dgammaMax = dgamma[j];

            if (dvMax < v[j])
                dvMax = v[j];

            if (drMax < r[j])
                drMax = r[j];

            sinSumm += sin(phase[j] + dzeta[j] - omega * (cascades[i].totalLength()) / v0);
            cosSumm += cos(phase[j] + dzeta[j] - omega * (cascades[i].totalLength()) / v0);

            if (qFuzzyIsNull(sinSumm))
                sinSumm = 0;

            if (qFuzzyIsNull(cosSumm))
                cosSumm = 0;
        }

        AOCC = sqrt(pow(2.0 / phaseCount * sinSumm, 2) + pow(2.0 / phaseCount * cosSumm, 2));

        NumericDetail cascadeDetail;
        cascadeDetail.dzeta0Ring = cascades[i].ringRadius() / v0;
        cascadeDetail.dzeta0L1 = cascades[i].leftTubeLength() / v0;
        cascadeDetail.dzeta0L2 = cascades[i].rightTubeLength() / v0;
        cascadeDetail.dgammaMax = dgammaMax;
        cascadeDetail.dvMax = dvMax;
        cascadeDetail.drMax = drMax;
        cascadeDetail.AOCC = AOCC;
        detailList.append(cascadeDetail);
    }

    delete[] v;
    delete[] r;
    delete[] dzeta;
    delete[] phase;
    delete[] dgamma;
    delete[] Um;

    detail = detailList;
    successful = true;

    return AOCC;
}

}  // Physics
