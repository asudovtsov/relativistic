#ifndef BEAM2DPAINTEDITEM_H
#define BEAM2DPAINTEDITEM_H

#include "beam.h"

#include <memory>

#include <QVariant>

#include <QQuickPaintedItem>

class Beam2DPaintedItem : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QVariant beam READ beam WRITE setBeam NOTIFY beamChanged)
    Q_PROPERTY(QSize particleSize READ particleSize WRITE setParticleSize NOTIFY particleSizeChanged)
    Q_PROPERTY(double particleRadius READ particleRadius WRITE setParticleRadius NOTIFY particleRadiusChanged)

public:
    explicit Beam2DPaintedItem(QQuickItem *parent = nullptr);

    void paint(QPainter *painter) override final;

    QVariant beam() const;
    void setBeam(const QVariant &beam);

    QSize particleSize() const;
    void setParticleSize(const QSize &particleSize);

    double particleRadius() const;
    void setParticleRadius(const double &particleRadius);

signals:
    void beamChanged();
    void particleSizeChanged();
    void particleRadiusChanged();

private:
    QVariant m_beam;
    QSize m_particleSize = QSize(1, 1);
    double m_particleRadius = 0.0;
};

Q_DECLARE_METATYPE(std::shared_ptr<Beam>)

#endif // BEAM2DPAINTEDITEM_H
