#include "qmlcalculator.h"
#include "calculator.h"

#include <QtQml>

#include <functional>

static QObject *singletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static QMLCalculator *calculator = new QMLCalculator;
    return calculator;
}

static void qmlRegister()
{
    qmlRegisterSingletonType<QMLCalculator>("relativistic", 1, 0, "Calculator", singletonProvider);
}

Q_COREAPP_STARTUP_FUNCTION(qmlRegister)

QMLCalculator::QMLCalculator(QObject *parent) : QObject(parent)
{
}

QMLCalculation *QMLCalculator::start(const QVariantMap &input,
                                     QMLCalculation::AnalysisType type,
                                     QMLCalculation::Mode mode)
{
    QMLCalculation *calculation = new QMLCalculation;
    if (Q_UNLIKELY(type == QMLCalculation::Invalid))
        return calculation;

    auto callback = std::bind(&QMLCalculation::onDataStateChanged, calculation);
    auto data = Calculator::makeCalculation(Physics::AnalysisType(type), callback);
    calculation->setData(data);
    calculation->start(CalculationUnit::fromVariantMap(input), mode);
    return calculation;
}
