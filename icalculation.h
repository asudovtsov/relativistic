#ifndef ICALCULATION_H
#define ICALCULATION_H

#include "physics.h"
#include "calculationunit.h"

#include <memory>

#include <functional>

class ICalculation : public std::enable_shared_from_this<ICalculation>
{
    friend class Calculator;

public:
    enum Mode
    {
        Synchronous = 0,
        Asynchronous = 1
    };

    enum State
    {
        Idle,
        Started,
        Finished,
        Error
    };

    virtual ~ICalculation();

    virtual Physics::AnalysisType analysisiType() const = 0;

    void start(const CalculationUnit &input, Mode mode = Synchronous);

    State state() const;
    QString errorText() const;

    CalculationUnit input() const;
    CalculationUnit output() const;

protected:
    typedef std::function<void()> StateChangeCallback;

    explicit ICalculation(const StateChangeCallback &);

    virtual CalculationUnit calculate(const CalculationUnit &input,
                                      bool &successful,
                                      QString &errorText) = 0;

private:
    void setState(State);

    State m_state;
    QString m_errorText;
    CalculationUnit m_input;
    CalculationUnit m_output;
    const StateChangeCallback m_callback;
};

#endif // ICALCULATION_H
