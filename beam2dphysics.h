#ifndef BEAM2DPHYSICS_H
#define BEAM2DPHYSICS_H

#include "beam.h"
#include "physics.h"

#include <algorithm>

class Buncher;

namespace Beam2DPhysics
{
    class Model
    {
        friend Model *makeBeamPhysicsModel(Physics::ModulationType modulationType,
                                           unsigned phaseCount,
                                           const Buncher &buncher);

    public:
        enum Section
        {
            LeftTube = 0,
            Ring = 1,
            RightTube = 2
        };

        unsigned phaseCount() const;
        unsigned cascadeCount() const;
        double r(unsigned phase, unsigned cascade) const;
        double v(unsigned phase, unsigned cascade) const;
        double t(unsigned phase, Section, unsigned cascade) const;
        double totalDzeta(unsigned phase) const;

    private:
        Model(unsigned phaseCount, unsigned cascadeCount);
        ~Model();
        Model(const Model &) = delete;
        Model(const Model &&) = delete;

        unsigned m_phaseCount;
        unsigned m_cascadeCount;
        double *m_r;
        double *m_v;
        double *m_t;
        double *m_dzetaTotal;
    };

    Model *makeBeamPhysicsModel(Physics::ModulationType modulationType,
                                unsigned phaseCount,
                                const Buncher &buncher);

    void model(int ns, const Beam &beam, Model *physicsModel);
}

#endif // BEAM2DPHYSICS_H
