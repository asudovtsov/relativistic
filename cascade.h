#ifndef CASCADE_H
#define CASCADE_H

#include <QList>

class Cascade
{
public:
    struct Modulation
    {
        double Um; // амплитуда входного модулирующего напряжения
        double M;  // коэффициент взаимодействия пучка с полем резонатора
        double Ro; // волновое сопротивление резонатора
        double Q;  // добротность резонатора
        double Y;  // частотная расстройка резонатора
    };

    Cascade();

    double leftTubeLength() const;
    void setLeftTubeLength(double leftTubeLength);

    double rightTubeLength() const;
    void setRightTubeLength(double rightTubeLength);

    double linearLength() const; // Сумма длин линейных участков
    double totalLength() const; // Сумма длин линейных участков и длины окружности поворотного кольца

    double ringRadius() const;
    void setRingRadius(double ringRadius);

    QList<Modulation> modulations() const;
    Modulation modulation(int harmonicNumber) const;
    void addModulation(const Modulation &modulation);
    void resetModulations();

private:
    double m_leftTubeLength;
    double m_rightTubeLength;
    double m_ringRadius;

    // параметры модуляции для k-ых частот, где k = m_modulations.index + 1
    QList<Modulation> m_modulations;
};

Q_DECLARE_METATYPE(QList<Cascade>)

#endif // CASCADE_H
