#ifndef QMLCASCADE_H
#define QMLCASCADE_H

#include "cascade.h"

#include <QObject>

class ModulatonGadget
{
    Q_GADGET
    Q_PROPERTY(double um READ Um WRITE setUm STORED false FINAL)
    Q_PROPERTY(double m READ M WRITE setM STORED false FINAL)
    Q_PROPERTY(double ro READ Ro WRITE setRo STORED false FINAL)
    Q_PROPERTY(double q READ Q WRITE setQ STORED false FINAL)
    Q_PROPERTY(double y READ Y WRITE setY STORED false FINAL)

public:
    ModulatonGadget(const Cascade::Modulation &modulation = Cascade::Modulation());

    inline operator Cascade::Modulation() const { return m_data; }

    double Um() const;
    double M() const;
    double Ro() const;
    double Q() const;
    double Y() const;

public slots:
    void setUm(const double &Um);
    void setM(const double &M);
    void setRo(const double &Ro);
    void setQ(const double &Q);
    void setY(const double &Y);

private:
    Cascade::Modulation m_data;
};

class CascadeGadget
{
    Q_GADGET
    Q_PROPERTY(double leftTubeLength READ leftTubeLength WRITE setLeftTubeLength STORED false FINAL)
    Q_PROPERTY(double rightTubeLength READ rightTubeLength WRITE setRightTubeLength STORED false FINAL)
    Q_PROPERTY(double ringRadius READ ringRadius WRITE setRingRadius STORED false FINAL)
    Q_PROPERTY(QVariantList modulations READ modulations WRITE setModulations STORED false FINAL)

public:
    CascadeGadget(const Cascade &data = Cascade());

    inline operator Cascade() const { return m_data; }

    double leftTubeLength() const;
    double rightTubeLength() const;
    double ringRadius() const;

    QVariantList modulations() const;
    Q_INVOKABLE QVariant modulation(int harmonicNumber) const;
    Q_INVOKABLE void addModulation(const ModulatonGadget &modulation);
    Q_INVOKABLE void resetModulations();

public slots:
    void setLeftTubeLength(const double &leftTubeLength);
    void setRightTubeLength(const double &rightTubeLength);
    void setRingRadius(const double &ringRadius);
    void setModulations(const QVariantList &modulation);

private:
    Cascade m_data;
};

Q_DECLARE_METATYPE(ModulatonGadget)
Q_DECLARE_METATYPE(CascadeGadget)

#endif // QMLCASCADE_H
