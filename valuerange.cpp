#include "valuerange.h"

ValueRange::ValueRange() :
    m_data(new RangeData)
{
    qRegisterMetaType<RangeType>("RangeType");
}

ValueRange::~ValueRange()
{
}

ValueRange::RangeType ValueRange::type() const
{
    return static_cast<RangeType>(m_data->type());
}

QVariant ValueRange::from() const
{
    return m_data->from();
}

void ValueRange::setFrom(const QVariant &from)
{
    m_data->setFrom(from);
}

QVariant ValueRange::to() const
{
    return m_data->to();
}

void ValueRange::setTo(const QVariant &to)
{
    m_data->setTo(to);
}

QVariant ValueRange::step() const
{
    return m_data->step();
}

void ValueRange::setStep(const QVariant &step)
{
    m_data->setStep(step);
}

QVariant ValueRange::next(const QVariant &value) const
{
    return m_data->next(value);
}

QVariant ValueRange::previous(const QVariant &value) const
{
    return m_data->previous(value);
}
