#ifndef CALCULATOR_H
#define CALCULATOR_H

#include "physics.h"

#include <QMetaType>

#include <memory>
#include <functional>

class ICalculation;

Q_DECLARE_METATYPE(std::shared_ptr<ICalculation>)

class CalculationUnit;

class Calculator
{
public:
    static std::shared_ptr<ICalculation>
    makeCalculation(Physics::AnalysisType type,
                    const std::function<void()> &callback = std::function<void()>());

private:
    Calculator() = delete;
    Calculator(const Calculator &) = delete;
    Calculator &operator=(const Calculator &) = delete;
};

#endif // CALCULATOR_H
