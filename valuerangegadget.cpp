#include "valuerangegadget.h"

ValueRangeGadget::ValueRangeGadget(const ValueRange &data) :
    m_data(data)
{
}

ValueRangeGadget::RangeType ValueRangeGadget::type() const
{
    return static_cast<RangeType>(m_data.type());
}

QVariant ValueRangeGadget::from() const
{
    return m_data.from();
}

QVariant ValueRangeGadget::to() const
{
    return m_data.to();
}

QVariant ValueRangeGadget::step() const
{
    return m_data.step();
}

void ValueRangeGadget::setFrom(const QVariant &from)
{
    m_data.setFrom(from);
}

void ValueRangeGadget::setTo(const QVariant &to)
{
    m_data.setTo(to);
}

void ValueRangeGadget::setStep(const QVariant &step)
{
    m_data.setStep(step);
}
