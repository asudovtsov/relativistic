#ifndef ANALYTICALCALCULATION_H
#define ANALYTICALCALCULATION_H

#include "icalculation.h"

class AnalyticalCalculation final : public ICalculation
{
    friend class Calculator;

public:
    Physics::AnalysisType analysisiType() const override final;

protected:
    explicit AnalyticalCalculation(const StateChangeCallback &callback = StateChangeCallback());

    CalculationUnit calculate(const CalculationUnit &input,
                              bool &successful,
                              QString &errorText) override final;
};

#endif // ANALYTICALCALCULATION_H
