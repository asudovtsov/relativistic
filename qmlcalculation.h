#ifndef QMLCALCULATION_H
#define QMLCALCULATION_H

#include "physics.h"
#include "icalculation.h"

#include <memory>
#include <QObject>

class QMLCalculation : public QObject
{
    friend class QMLCalculator;

    Q_OBJECT
    Q_PROPERTY(bool valid READ isValid STORED false FINAL)
    Q_PROPERTY(AnalysisType analysisType READ analysisType FINAL)
    Q_PROPERTY(QString errorText READ errorText NOTIFY errorTextChanged STORED false FINAL)
    Q_PROPERTY(State state READ state NOTIFY stateChanged STORED false FINAL)

public:
    enum AnalysisType
    {
        Analytical = Physics::Analytical,
        Numerical = Physics::Numerical,
        Invalid
    };
    Q_ENUM(AnalysisType)

    enum Mode
    {
        Synchronous = ICalculation::Synchronous,
        Asynchronous = ICalculation::Asynchronous
    };
    Q_ENUM(Mode)

    enum State
    {
        Null = ICalculation::Idle,
        Started = ICalculation::Started,
        Finished = ICalculation::Finished,
        Error = ICalculation::Error
    };
    Q_ENUM(State)

    bool isValid() const;

    void start(const CalculationUnit &input, Mode mode = Synchronous);

    AnalysisType analysisType() const;
    QString errorText() const;
    State state() const;

    CalculationUnit input() const;
    CalculationUnit output() const;

signals:
    void errorTextChanged();
    void stateChanged();

private:
    explicit QMLCalculation(QObject *parent = nullptr);
    void setData(std::shared_ptr<ICalculation> data);
    void onDataStateChanged();

    std::shared_ptr<ICalculation> m_data;
    AnalysisType m_analysisType;
};

Q_DECLARE_METATYPE(QMLCalculation::AnalysisType)
Q_DECLARE_METATYPE(QMLCalculation::Mode)
Q_DECLARE_METATYPE(QMLCalculation::State)

#endif // QMLCALCULATION_H
